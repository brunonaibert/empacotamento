#  Requisitos

Instalando pacotes necessários para empacotamento:
```
apt update
apt install docker.io debootstrap
```

Com usuário comum, criar chave GPG:
```
gpg --list-keys
gpg --full-gen-key
  1
  4096
  2y
  y
  Nome completo
  E-mail


  O
  senha_forte
```

```
gpg --gen-revoke nr_da_chave > ~/.gnupg/revocation-nr_da_chave.crt
  y
  0


  y
```

Edite o arquivo `~/.gnupg/gpg.conf`:
```
keyserver hkp://keys.openpgp.org
```

```
gpg --send-key nr_da_chave
```

```
gpg-key2ps -p a4 nr_da_chave | gs -sDEVICE=pdfwrite -sOutputFile=impresso-fingerprint-gnupg.pdf
```

```
gpg -a --export nr_da_chave > /tmp/nr_da_chave.pub
gpg -a --export-secret-keys nr_da_chave > /tmp/nr_da_chave.key
```