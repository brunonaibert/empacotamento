# Jaula

```
debootstrap sid /tmp/jaula-sid http://deb.debian.org/debian
tar -C /tmp/jaula-sid -c . | docker import - base-sid:20240103
docker run --name=jaula -ti base-sid:20240103 /bin/bash
```

Dentro do container jaula, realize a seguinte configuração:
```
apt update
apt install vim
```

Edite o arquivo `~/.vimrc`:
```
source $VIMRUNTIME/defaults.vim
set mouse-=a
set cc=80
```

Edite o arquivo `/etc/bash.bashrc`:
```
alias ls='ls --color=auto'
alias tree='tree -aC'
alias debuildsa='dpkg-buildpackage -sa -knr_da_chave'
alias uscan-check='uscan --verbose --report'
alias debcheckout='debcheckout -a'
export DEBFULLNAME="Nome completo"
export DEBEMAIL="email"
export EDITOR=vim
export LANG=C.UTF-8
export LANGUAGE=C.UTF-8
export LC_ALL=C.UTF-8
export QUILT_PATCHES=debian/patches
export PS1='JAULA-SID-\u@\h:\w\$ '
cd /PKGS
```

Anotação: Outros pacotes a se instalar, avaliar: locales, renameutils, splitpatch, wget. Se locales for instalado, necessário `dpkg-reconfigure locales`.

```
mkdir /PKGS
source /etc/bash.bashrc
apt install autopkgtest blhc devscripts dh-make dput-ng git-buildpackage how-can-i-help license-detector quilt spell tardiff tree
dpkg-reconfigure tzdata
apt clean
```

Edite o arquivo `~/.lintianrc`:
```
display-info = yes
pedantic = yes
display-experimental = yes
color = always
```

Edite o arquivo `/etc/apt/sources.list` (adicione ao final do arquivo):
```
deb-src http://deb.debian.org/debian sid main
```

Edite o arquivo `/etc/git-buildpackage/gbp.conf` (descomente na sessão [DEFAULT]):
```
debian-branch = debian/master
pristine-tar = True
```

Em um terminal, fora do container jaula:
```
docker cp /tmp/nr_da_chave.key jaula:/tmp/
docker cp /tmp/nr_da_chave.pub jaula:/tmp/
rm /tmp/nr_da_chave.*
```

De volta ao terminal que está do container jaula:
```
gpg --list-keys
echo "pinentry-mode loopback" >> ~/.gnupg/gpg.conf
gpg --import /tmp/nr_da_chave.key /tmp/nr_da_chave.pub
rm /tmp/nr_da_chave.*
```

Edite o arquivo `/etc/devscripts.conf`:
```
DEBSIGN_KEYID=nr_da_chave
```

Saia do container jaula.

```
docker commit jaula jaula-sid:20240103-1
```

