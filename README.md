# Empacotamento

Anotações sobre o aprendizado com empacotamento de software para o Debian.

## Tópicos

* [Requisitos](./requisitos.md)
* [Jaula](./jaula.md)

## Ligações externas

* [Debianet](https://debianet.com.br/)